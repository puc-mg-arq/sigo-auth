module cmd/sigo-auth/main.go

go 1.15

require (
	github.com/go-chi/chi v1.5.0
	github.com/go-redis/redis/v8 v8.4.0
	github.com/google/uuid v1.1.2
	github.com/jinzhu/gorm v1.9.16
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.3.0 // indirect
	github.com/sirupsen/logrus v1.7.0
	go.mongodb.org/mongo-driver v1.4.3
	gorm.io/gorm v1.20.12
)
