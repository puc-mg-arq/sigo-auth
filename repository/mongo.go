package repository

import (
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

type MongoDB struct {
	Database *mongo.Database
}

func NewMongoDBClient(host string, user string, pwd string, db string, port string, minPoolSize uint64, connectTimeout time.Duration) (*MongoDB, error) {

	//mongodb+srv://sigo:<password>@sigo-auth.ggqhx.mongodb.net/sigo-auth?retryWrites=true&w=majority
	//uri := fmt.Sprintf("mongodb://%s:%s@%s:%s/%s", user, pwd, host, port, db)
	uri := fmt.Sprintf("mongodb+srv://%s:%s@%s/%s", user, pwd, host, db)

	//fmt.Println(fmt.Sprintf("uri: %s", uri))
	// Set client options
	var retryWrites = false
	clientOptions := &options.ClientOptions{
		MinPoolSize: &minPoolSize,
		ConnectTimeout: &connectTimeout,
		RetryWrites: &retryWrites,
	}
	clientOptions.ApplyURI(uri)

	// Connect to MongoDB
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		return nil, err
	}

	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}
	// Check the connection
	err = client.Ping(ctx, nil)
	if err != nil {
		return nil, err
	}

	database := client.Database(db)

	mongoDB := &MongoDB{
		Database: database,
	}

	return mongoDB, nil
}
