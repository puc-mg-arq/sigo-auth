package repository

import (
	"cmd/sigo-auth/main.go/model"
	"cmd/sigo-auth/main.go/util"
	"fmt"
	"time"
)

const (
	MongoDBCollectionToken = "Token"
	RedisTokenKey          = "SIGO_AUTH_TOKEN"
	RedisTokenKeyTTL       = time.Minute * 7
	PostgresTableToken     = "\"sigo_auth\"" + "." + "\"token\""
)

type Token struct {
	Redis      *Redis
	MongoDB    *MongoDB
	PostgresDB *PostgresDB
	Logging    util.Logging
}

func (repository *Token) RedisSave(token string) {
	repository.Redis.Set(fmt.Sprintf("%s:%s", RedisTokenKey, token), token, RedisTokenKeyTTL)
}

func (repository *Token) RedisGet(token string) string {

	token = repository.Redis.Get(fmt.Sprintf("%s:%s", RedisTokenKey, token))

	return token
}

func (repository *Token) MongoDBSave(token *model.Token) error {

	//obj, err := repository.MongoDB.Database.Collection(MongoDBCollectionToken).InsertOne(ctx, token)
	_, err := repository.MongoDB.Database.Collection(MongoDBCollectionToken).InsertOne(ctx, token)

	if err != nil {
		return err
	}

	//token.ID = obj.InsertedID.(primitive.ObjectID)

	return nil
}

func (repository *Token) PostgresDBDBSave(token *model.Token) error {

	if err := repository.PostgresDB.DB.Table(PostgresTableToken).Create(token).Error; err != nil {
		return err
	}

	return nil
}
