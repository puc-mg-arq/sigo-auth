package repository

import (
	"cmd/sigo-auth/main.go/util"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"time"
)

type PostgresDB struct {
	DB *gorm.DB
}

const (
	DBUser     = "sigo"
	DBPassword = "sigo1234"
	DBName     = "sigo"
	DBHost     = "192.168.0.100"
	DBPort     = "5432"
	DBType     = "postgres"
)

func GetDBType() string {
	return DBType
}

func GetPostgresConnectionString() string {
	dataBase := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		DBHost,
		DBPort,
		DBUser,
		DBName,
		DBPassword)
	return dataBase
}

func NewDB(logging util.Logging) *PostgresDB {
	var err error
	conString := GetPostgresConnectionString()

	log.Print(conString)

	DB, err := connecting(logging, conString)
	if err != nil {
		log.Panic(err)
	}

	postgresDB := PostgresDB{
		DB: DB,
	}

	return &postgresDB
}

func connecting(logging util.Logging, conString string) (*gorm.DB, error) {

	tryConnect := 1

	for {
		logging.LoggerMessage("tentativa inicializar postgres db", "tentativa", tryConnect)
		DB, err := gorm.Open(GetDBType(), conString)
		if err != nil && tryConnect != 3 {

			tryConnect++
			if tryConnect > 3 {
				logging.LoggerMessage("error ao inicializar postgres db", "tentativas para inicializar", tryConnect)
				return nil, err
			}

			time.Sleep(3 * time.Second)
			continue
		}

		return DB, err
	}
}