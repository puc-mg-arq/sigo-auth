package repository

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
	"time"
)

var ctx = context.Background()

type Redis struct {
	Addr        string
	Password    string
	DB          int
	RedisClient *redis.Client
	Logger      *logrus.Entry
}

func (r *Redis) NewClient() {

	rdb := redis.NewClient(&redis.Options{
		Addr:               r.Addr,
		Password:           r.Password, // no password set
		DB:                 r.DB,       // use default DB
		PoolSize:           100,
		ReadTimeout:        2 * time.Second,
	})

	pong, err := rdb.Ping(ctx).Result()
	fmt.Println(fmt.Sprintf("pong: %v; err: %v", pong, err))
	// Output: PONG <nil>

	r.RedisClient = rdb
}

func (r *Redis) Set(hash string, key string, expirationTime time.Duration) {

	//r.RedisClient.MSetNX(ctx, key, "", expirationTime)
	_ , err := r.RedisClient.Set(ctx, hash, key, expirationTime).Result()
	if err != nil {
		fmt.Printf("Error: %s", err.Error())
		//r.Logger.Warningf("It was not possible insert value in redis", "err", err.Error())
		return
	}

}

func (r *Redis) Get(key string) string {

	//key = fmt.Sprintf("%s:%s", r.Hash, key)
	value, err := r.RedisClient.Get(ctx, key).Result()
	if err != nil {

		//r.Logger.Warningf("It was not possible get value in redis", "err", err.Error())
		return ""
	}

	return value
}
