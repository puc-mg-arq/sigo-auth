package repository

import (
	"cmd/sigo-auth/main.go/model"
	"cmd/sigo-auth/main.go/util"
)

const (
	MongoDBCollectionUser = "User"
	PostgresTableUser     = "sigo_auth.user_auth"
)

type User struct {
	Redis      *Redis
	MongoDB    *MongoDB
	PostgresDB *PostgresDB
	Logging    util.Logging
}

func (repo *User) Create(user *model.User) error {

	//obj, err := repo.MongoDB.Database.Collection(MongoDBCollectionUser).InsertOne(ctx, user)
	//_, err := repo.MongoDB.Database.Collection(MongoDBCollectionUser).InsertOne(ctx, user)
	//if err != nil {
	//	return err
	//}

	//user.ID = obj.InsertedID.(primitive.ObjectID)

	if err := repo.PostgresDB.DB.Table(PostgresTableUser).Create(user).Error; err != nil {
		return err
	}

	return nil
}

func (repo *User) GetByUserAndKey(user string, key string) (*model.User, error) {

	var u model.User
	//filter := bson.M{"user": user, "key": key}
	//repo.MongoDB.Database.Collection(MongoDBCollectionUser).FindOne(ctx, filter).Decode(&u)

	repo.Logging.LoggerMessage("teste table", "PostgresTableUser", PostgresTableUser)
	//result := repo.PostgresDB.DB.Table("sigo_auth.user_auth").
	//	Where(`username = ? and key = ?`, user, key).First(&u)
	result := repo.PostgresDB.DB.Raw("select usera.* from sigo_auth.user_auth usera " +
		" where usera.username = ? and usera.key = ? ORDER BY usera.id ASC LIMIT 1", user, key).Scan(&u)

	return &u, result.Error
}

func (repo *User) GetByUser(user string) (*model.User, error) {

	var u model.User
	//filter := bson.M{"user": user}
	//repo.MongoDB.Database.Collection(MongoDBCollectionUser).FindOne(ctx, filter).Decode(&u)

	result := repo.PostgresDB.DB.Table(PostgresTableUser).Where("username = ?", user).First(&u)

	return &u, result.Error
}
