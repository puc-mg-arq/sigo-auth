# SIGO Authorization

Projeto que faz a autencicacao e a autorizacao das requisicoes do sistema SIGO.

## Comunicacao

### Login

Efetuar login no sistema.

#### Request

* GET /sigo-auth/login/ HTTP/1.1
* Host: localhost:5000
* Headers:
    * x-sigo-user: [USER]
    * x-sigo-key: [KEY]
    
ex.:

```
curl -vvv --location --request GET 'http://localhost:5000/sigo-auth/login/' --header 'x-sigo-user: [USUARIO]' --header 'x-sigo-key: [KEY]' 
```

#### Response

* Status Code: 200 - OK
* Body:

```
{
    "result": "[TOKEN]",
    "user": {
        "id": [USUARIO_ID],
        "name": "[NOME]",
        "user": "[NOME_USUARIO]",
        "active": [true/false]
    }
}
```

### Criar Usuario

Criar usuario no sistema.

#### Request

* POST /sigo-auth/user/ HTTP/1.1
* Host: localhost:5000
* Header:
    * x-sigo-token: [TOKEN]
    * x-sigo-user: [USER]
    
ex.:

```
curl -vvv --location --request POST 'http://localhost:5000/sigo-auth/user/'  --header 'x-sigo-user: [USER]' --header 'x-sigo-token: [TOKEN]' --header 'Content-Type: application/json' --data-raw '{
    "user":"teste01",
    "name": "teste 01",
    "key":"12345"
}'
```

#### Response

* Status Code: 200 - OK
* Body:

```
{
    "result": {
        "id": [USUARIO_ID],
        "name": "[NOME]",
        "user": "[NOME_USUARIO]",
        "active": [true/false]
    }
}
```

* Status Code: 401 - Unauthorized
* Body:

```
{
  "error":"invalid token"
}
```

### Solicitar Token

Solicitar um novo token.

#### Request

* GET /sigo-auth/token/ HTTP/1.1
* Host: localhost:5000
* Header:
  * x-sigo-token: [TOKEN]
  * x-sigo-user: [USER]
  
ex.:

```
curl -vvv --location --request GET 'http://localhost:5000/sigo-auth/token/' --header 'x-sigo-user: [USUARIO]' --header 'x-sigo-token: [TOKEN]'
```

#### Response

* Status Code: 200 - OK
* Body:

```
{
  "result":"[TOKEN]"
}
```

* Status Code: 401 - Unauthorized
* Body:

```
{
  "error":"invalid token"
}
```

### Validar Token

Validar a veracidade do token.

#### Request

* GET /sigo-auth/token/valid HTTP/1.1
* Host: localhost:5000
* Header:
  x-sigo-token: [TOKEN]
  x-sigo-user: [USER]

ex.:

```
curl -vvv --location --request GET 'http://localhost:5000/sigo-auth/token/valid' --header 'x-sigo-user: [USUARIO]' --header 'x-sigo-token: [TOKEN]'
```

#### Response

* Status Code: 200 - OK
* Body:

```
{
  "result":"[TOKEN]"
}
```

* Status Code: 401 - Unauthorized
* Body:

```
{
  "error":"invalid token"
}
```
