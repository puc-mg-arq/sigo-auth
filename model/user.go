package model

import (
	"time"
)

//https://www.mongodb.com/blog/post/quick-start-golang--mongodb--modeling-documents-with-go-data-structures
type User struct {
	//ID          primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	//Name        string             `bson:"name,omitempty" json:"name"`
	//User        string             `bson:"user,omitempty" json:"user"`
	//Key         string             `bson:"key,omitempty" json:"key"`
	//DateCreated time.Time          `bson:"dateCreated,omitempty" json:"dateCreated"`
	//IsActive    bool               `bson:"active,omitempty" json:"active"`

	ID          int64     `json:"id" gorm:"column:id"`
	Name        string    `json:"name" gorm:"column:name"`
	User        string    `json:"user" gorm:"column:username"`
	Key         string    `json:"key" gorm:"column:key"`
	DateCreated time.Time `json:"dateCreated" gorm:"column:date_created"`
	IsActive    bool      `json:"active" gorm:"column:active"`
}

type UserResponse struct {
	//ID       primitive.ObjectID `json:"id"`
	ID       int64  `json:"id"`
	Name     string `json:"name"`
	User     string `json:"user"`
	IsActive bool   `json:"active"`
}

func (user *User) ToUserResponse() UserResponse {
	return UserResponse{
		ID:       user.ID,
		Name:     user.Name,
		User:     user.User,
		IsActive: user.IsActive,
	}
}
