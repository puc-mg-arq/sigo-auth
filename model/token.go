package model

import (
	"time"
)

type Token struct {
	//gorm.Model
	//ID          primitive.ObjectID `bson:"_id,omitempty" json:"id" gorm:"column:id"`
	//Token       string    `bson:"token,omitempty" json:"token" gorm:"column:token"`
	//UserID      string    `bson:"userID,omitempty" json:"userID" gorm:"column:user_id"`
	//DateCreated time.Time `bson:"dateCreated,omitempty" json:"dateCreated" gorm:"column:date_created"`

	ID          int64     `json:"id" gorm:"column:id"`
	Token       string    `json:"token" gorm:"column:token"`
	UserID      int64     `json:"userID" gorm:"column:user_id"`
	DateCreated time.Time `json:"dateCreated" gorm:"column:date_created"`
}
