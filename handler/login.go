package handler

import (
	"cmd/sigo-auth/main.go/service"
	"cmd/sigo-auth/main.go/util"
	"encoding/json"
	"fmt"
	"net/http"
)

type Login struct {
	TokenService *service.Token
	UserService  *service.User
	Logging      util.Logging
}

func (handler *Login) Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	userLogin := r.Header.Get("x-sigo-user")
	keyLogin := r.Header.Get("x-sigo-key")

	fmt.Println(fmt.Sprintf("Login request user: %s", userLogin))

	user, err := handler.UserService.Login(userLogin, keyLogin)
	if err != nil {
		fmt.Println(fmt.Sprintf("Error on login request user: %s", userLogin))
		marshal, _ := json.Marshal(map[string]interface{}{"error": err.Error()})

		w.WriteHeader(http.StatusBadRequest)
		w.Write(marshal)

		return
	}

	// gerar token pro usuario
	token, err := handler.TokenService.Create(user)
	if err != nil {

		fmt.Println(fmt.Sprintf("Error on login when token generation: %s", userLogin))
		marshal, _ := json.Marshal(map[string]interface{}{"error": err.Error()})

		w.WriteHeader(http.StatusBadRequest)
		w.Write(marshal)

		return
	}

	fmt.Println(fmt.Sprintf("Success on login request user: %s", userLogin))

	w.WriteHeader(http.StatusOK)
	marshal, _ := json.Marshal(map[string]interface{}{"result": token, "user": user.ToUserResponse()})
	w.Write(marshal)
}
