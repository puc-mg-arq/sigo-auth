package handler

import (
	"cmd/sigo-auth/main.go/model"
	"cmd/sigo-auth/main.go/service"
	"cmd/sigo-auth/main.go/util"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type User struct {
	UserService *service.User
	Logging    util.Logging
}

func (handler *User) Create(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		marshal, _ := json.Marshal(map[string]string{"error": err.Error()})
		w.WriteHeader(http.StatusBadRequest)
		w.Write(marshal)
		return
	}

	var user model.User
	json.Unmarshal(body, &user)

	err = handler.UserService.Create(&user)
	if err != nil {
		marshal, _ := json.Marshal(map[string]interface{}{"error": err.Error()})
		w.WriteHeader(http.StatusBadRequest)
		w.Write(marshal)

		return
	}

	w.WriteHeader(http.StatusOK)
	marshal, _ := json.Marshal(map[string]model.UserResponse{"result": user.ToUserResponse()})
	w.Write(marshal)

}
