package handler

import (
	"cmd/sigo-auth/main.go/service"
	"cmd/sigo-auth/main.go/util"
	"encoding/json"
	"net/http"
)

type Token struct {
	TokenService *service.Token
	Logging      util.Logging
}

func (handler *Token) Create(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	user := r.Header.Get("x-sigo-user")

	tokenKey, err := handler.TokenService.CreateByLogin(user)
	if err != nil {
		marshal, _ := json.Marshal(map[string]interface{}{"error": err.Error()})
		w.WriteHeader(http.StatusBadRequest)
		w.Write(marshal)

		return
	}

	w.WriteHeader(http.StatusOK)
	marshal, _ := json.Marshal(map[string]string{"result": tokenKey})
	w.Write(marshal)
}

func (handler *Token) Valid(w http.ResponseWriter, r *http.Request) {

	tokenToValid := r.Header.Get("x-sigo-token")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	marshal, _ := json.Marshal(map[string]string{"result": tokenToValid})
	w.Write(marshal)
}
