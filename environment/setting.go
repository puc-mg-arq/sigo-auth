package environment

import (
	"encoding/json"
	"time"
)

type setting struct {
	Server struct {
		Context string `envconfig:"SERVER_CONTEXT" default:"sigo-auth"`
		Port    string `envconfig:"PORT" default:"5000" required:"true" ignored:"false"`
	}
	Connection struct {
		MaxIdleConnections int `envconfig:"CONNECTION_MAX_IDLE_CONNECTIONS" default:"100"`
		RequestTimeout     int `envconfig:"CONNECTION_REQUEST_TIMEOUT" default:"2000"`
	}
	Redis struct {
		Addr     string `envconfig:"REDIS_ADDR"`
		Password string `envconfig:"REDIS_PASSWORD"`
		DB       int    `envconfig:"REDIS_DB" default:"0"`
	}
	MongoDB struct {
		//https://golangdocs.com/mongodb-golang
		// uri: mongodb://<dbuser>:<dbpassword>@ds117829.mlab.com:17829/sigo-auth
		//mongodb+srv://sigo:<password>@sigo-auth.ggqhx.mongodb.net/sigo-auth?retryWrites=true&w=majority
		Host           string        `envconfig:"SIGO_MONGO_HOST"`
		User           string        `envconfig:"SIGO_MONGO_USER"`
		Password       string        `envconfig:"SIGO_MONGO_PASSWORD"`
		DB             string        `envconfig:"SIGO_MONGO_DB"`
		Port           string        `envconfig:"SIGO_MONGO_PORT"`
		MinPoolSize    uint64        `envconfig:"SIGO_MONGO_MIN_POOL_SIZE" default:"20"`
		ConnectTimeout time.Duration `envconfig:"SIGO_MONGO_CONNECT_TIMEOUT" default:"3s"`
	}
}

var Setting setting

func (set *setting) GetJson() []byte {

	data, err := json.Marshal(set)
	if err != nil {
		data, _ = json.Marshal(setting{})
	}

	return data
}
