package main

import (
	"cmd/sigo-auth/main.go/environment"
	"cmd/sigo-auth/main.go/handler"
	"cmd/sigo-auth/main.go/middleware"
	"cmd/sigo-auth/main.go/repository"
	"cmd/sigo-auth/main.go/service"
	"cmd/sigo-auth/main.go/util"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/kelseyhightower/envconfig"
	"log"
	"net/http"
	"os"
	"time"
)

func init() {

	err := envconfig.Process("setting", &environment.Setting)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(fmt.Sprintf("%s", environment.Setting.GetJson()))

}

var logging util.Logging

func main() {

	logger := log.New(os.Stdout, "", log.Flags())
	logging = util.Logging{
		Logger: logger,
	}

	redis := &repository.Redis{
		Addr:     environment.Setting.Redis.Addr,
		Password: environment.Setting.Redis.Password,
		DB:       environment.Setting.Redis.DB,
	}

	redis.NewClient()

	//mongoDBClient, err := repository.NewMongoDBClient(environment.Setting.MongoDB.Host,
	//	environment.Setting.MongoDB.User, environment.Setting.MongoDB.Password, environment.Setting.MongoDB.DB,
	//	environment.Setting.MongoDB.Port, environment.Setting.MongoDB.MinPoolSize, environment.Setting.MongoDB.ConnectTimeout)

	//if err != nil {
	//	fmt.Println(fmt.Sprintf("MongoDB Connection. err: %s", err.Error()))
	//	panic(err.Error())
	//}

	postgresDB := repository.NewDB(logging)

	tokenRepository := &repository.Token{
		Redis:   redis,
		//MongoDB: mongoDBClient,
		PostgresDB: postgresDB,
		Logging: logging,
	}

	userRepository := &repository.User{
		//MongoDB: mongoDBClient,
		Redis:   redis,
		PostgresDB: postgresDB,
		Logging: logging,
	}

	userService := &service.User{
		UserRepository: userRepository,
		Logging: logging,
	}
	userHandler := handler.User{
		UserService: userService,
		Logging: logging,
	}

	tokenService := &service.Token{
		TokenRepository: tokenRepository,
		UserService: userService,
		Logging: logging,
	}

	tokenHandler := handler.Token{
		TokenService: tokenService,
		Logging: logging,
	}

	tokenMiddleware := middleware.Token{
		TokenService: tokenService,
		Logging: logging,
	}
	loginHandler := handler.Login{
		TokenService: tokenService,
		UserService: userService,
		Logging: logging,
	}

	router := chi.NewRouter()
	context := environment.Setting.Server.Context

	router.With(tokenMiddleware.Valid).Route(fmt.Sprintf("/%s/token", context), func(router chi.Router) {
		router.Get(fmt.Sprintf("/"), tokenHandler.Create)
		router.Get(fmt.Sprintf("/valid"), tokenHandler.Valid)
	})

	router.Route(fmt.Sprintf("/%s/user", context), func(router chi.Router) {
		router.Use(tokenMiddleware.Valid)
		router.Post(fmt.Sprintf("/"), userHandler.Create)
	})

	router.Route(fmt.Sprintf("/%s/login", context), func(router chi.Router) {
		router.Get(fmt.Sprintf("/"), loginHandler.Login)
	})

	serverHttp := &http.Server{
		Addr:           fmt.Sprintf(":%s", environment.Setting.Server.Port),
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	fmt.Println(fmt.Sprintf("Server started in Port: %s - Context: %s", serverHttp.Addr, context))
	if err := serverHttp.ListenAndServe(); err != nil {
		fmt.Println("Listen and Serve", "err", err.Error())
		panic(err.Error())
	}

}
