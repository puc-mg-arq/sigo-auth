package middleware

import (
	"cmd/sigo-auth/main.go/service"
	"cmd/sigo-auth/main.go/util"
	"encoding/json"
	"net/http"
)

type Token struct {
	TokenService *service.Token
	Logging      util.Logging
}

func (token *Token) Valid(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		tokenToValid := r.Header.Get("x-sigo-token")

		_, err := token.TokenService.Valid(tokenToValid)
		if err != nil {
			marshal, _ := json.Marshal(map[string]interface{}{"error": err.Error()})

			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			w.Write(marshal)

			return
		}

		//ctx := r.Context()
		//perm, ok := ctx.Value("acl.permission").(YourPermissionType)
		//if !ok || !perm.IsAdmin() {
		//	http.Error(w, http.StatusText(403), 403)
		//	return
		//}
		next.ServeHTTP(w, r)
	})
}
