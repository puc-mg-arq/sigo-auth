package service

import (
	"cmd/sigo-auth/main.go/model"
	"cmd/sigo-auth/main.go/repository"
	"cmd/sigo-auth/main.go/util"
	"errors"
	"time"
)

type User struct {
	UserRepository *repository.User
	Logging    util.Logging
}

func (service *User) Create(user *model.User) error {

	err := service.createValidate(user)
	if err != nil {
		return err
	}

	if user.ID == 0 {
		user.DateCreated = time.Now()
		user.IsActive = true
	}

	service.UserRepository.Create(user)

	return nil
}

func (service *User) createValidate (user *model.User) error {

	if user == nil || len(user.User) == 0 || len(user.Key) == 0 || len(user.Name) == 0{
		return errors.New("login, name or user are invalid")
	}

	return nil
}

func (service *User) Login(user string, key string) (*model.User, error) {

	userLogged, err := service.UserRepository.GetByUserAndKey(user, key)
	if err != nil {
		service.Logging.LoggerMessage("Error to get user by username and key", "error", err.Error())
		return nil, err
	}

	if userLogged == nil || userLogged.IsActive == false {
		return nil, errors.New("user invalid")
	}

	return userLogged, nil
}

func (service *User) GetByUser(user string) (*model.User, error) {

	userLogged, err := service.UserRepository.GetByUser(user)
	if err != nil {
		return nil, err
	}

	if userLogged == nil || userLogged.IsActive == false {
		return nil, errors.New("user invalid")
	}

	return userLogged, nil
}
