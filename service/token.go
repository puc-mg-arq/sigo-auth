package service

import (
	"cmd/sigo-auth/main.go/model"
	"cmd/sigo-auth/main.go/repository"
	"cmd/sigo-auth/main.go/util"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"time"
)

type Token struct {
	UserService *User
	TokenRepository *repository.Token
	Logging    util.Logging
}

func (service *Token) CreateByLogin (userLogin string) (string, error) {

	user, err := service.UserService.GetByUser(userLogin)
	if err != nil {
		return "", err
	}

	return service.Create(user)
}

func (service *Token) Create (user *model.User) (string, error) {

	token := model.Token{
		Token:       fmt.Sprintf("%s.%d",uuid.New().String(), time.Now().UnixNano()) ,
		UserID:      user.ID,
		DateCreated: time.Now(),
	}

	service.TokenRepository.RedisSave(token.Token)

	//https://golangbyexample.com/goroutines-golang/
	go func() {
		//service.TokenRepository.MongoDBSave(&token)
		service.TokenRepository.PostgresDBDBSave(&token)
	} ()

	return token.Token, nil
}

func (service *Token) Valid (token string) (bool, error) {

	get := service.TokenRepository.RedisGet(token)
	if len(get) > 0 {
		return true, nil
	}

	return false, errors.New("invalid token")
}